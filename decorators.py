import time
import os

PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        print(f'Execution time for {method.__name__}(): {((te - ts) * 1000):.4f} ms')
        return result
    return timed


def clean_file(filename):
    def real_decorator(function):
        def wrapper(*args, **kwargs):
            with open(os.path.join(PROJECT_DIR, filename), 'w'):
                pass
            function(*args, **kwargs)
        return wrapper
    return real_decorator
