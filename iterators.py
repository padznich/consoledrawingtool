class ObjectIterator:
    def __init__(self, objects: list):
        self.i = 0
        self.objects = objects
        self.n = len(objects)

    def __iter__(self):
        return self

    def __next__(self):
        if self.i < self.n:
            i = self.i
            self.i += 1
            return self.objects[i]
        else:
            raise StopIteration()
