import argparse

from decorators import clean_file
import utils


def draw(input_filename: str, output_filename: str):
    print('Start drawing.')
    lines = utils.read_file(input_filename)
    # Use decorator with arguments for cleaning output file if it exist
    clean_file(output_filename)(utils.parse_file_lines)(lines, filename=output_filename)
    print('Finish drawing.')


def main():
    parser = argparse.ArgumentParser(description='Console Draw Tool')
    parser.add_argument('input_filename', type=str, help="input file name.")
    parser.add_argument('--output_filename', type=str, help="ountput file name. Default - 'samples/output.txt'",
                        default='samples/output.txt')
    args = parser.parse_args()
    draw(input_filename=args.input_filename, output_filename=args.output_filename)


if __name__ == '__main__':
    main()
