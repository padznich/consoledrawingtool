class OnlyStraightLineMixin:
    def __new__(cls, point1, point2):
        if point1.x == point2.x or point1.y == point2.y:
            return super().__new__(cls)
        raise Exception('Only straight line available.')


class ValidateRectangleMixin:
    def __new__(cls, point1, point2):
        # Check if rectangle is not line
        if point1.x != point2.x and point1.y != point2.y:
            # Check if point1 is left upper point
            if point1.x < point2.x and point1.y < point2.y:
                return super().__new__(cls)
        raise AssertionError('Not valid points for rectangle.')


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]
