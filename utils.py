import os

from decorators import PROJECT_DIR
from shapes import Point, Canvas, Rectangle, Line


def read_file(filename: str) -> list:
    """
    Read non empty lines from input file.
    """
    filepath = os.path.join(PROJECT_DIR, filename)
    with open(filepath) as file:
        # skip empty lines
        lines = filter(None, (line.rstrip() for line in file.readlines()))
    return lines


def parse_file_lines(lines: list, filename: str):
    """
    Parse lines from file. Separate shapes letters from attributes.
    """
    for line_number, line in enumerate(lines):
        command, params = line.split(maxsplit=1)
        params = params.strip().split()
        # check if first command is draw canvas
        if line_number == 0:
            if command == 'C':
                # create canvas, Canvas is a singletone so now we can just use Canvas() for getting existing instance
                Canvas(*params)
                write_to_file(filename=filename)
                continue
            raise Exception("First command should be 'C'.")
        parse_commands(command, params, filename)


def parse_commands(command: str, params: list, filename: str):
    """
    Mapping letters from file to shapes, create shapes with given params
    and write shapes to canvas
    """
    if command == 'L':
        line = Line(Point(*params[0:2]), Point(*params[2:4]))
        Canvas().draw_line(line)
    elif command == 'R':
        rectangle = Rectangle(Point(*params[0:2]), Point(*params[2:4]))
        Canvas().draw_rectangle(rectangle)
    elif command == 'B':
        Canvas().bucket_fill(Point(*params[:2]), params[2])
    else:
        raise Exception(f'Not valid command: {command}')
    write_to_file(filename)


def write_to_file(filename: str):
    """
    Append new canvas to file
    """
    filepath = os.path.join(PROJECT_DIR, filename)
    with open(filepath, 'a') as file:
        file.write(str(Canvas()))
